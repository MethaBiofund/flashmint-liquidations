let Flashloan = artifacts.require("Flashloan")

module.exports = async function (deployer, network) {
    try {

        let lendingPoolAddressesProviderAddress;
        let lendingPoolAddressesProviderAddressV2 = "0xB53C1a33016B2DC2fF3653530bfF1848a515c8c5";
        let liquidableAssetAddress = "0xA0b86991c6218b36c1d19D4a2e9Eb0cE3606eB48";  // Replace with the address of the liquidable asset.

        switch(network) {
            case "mainnet":
                lendingPoolAddressesProviderAddress = "0x24a42fD28C976A61Df5D00D0599C34c4f90748c8"; break
            case "mainnet-fork":
            case "development": // For Ganache mainnet forks
                lendingPoolAddressesProviderAddress = "0x24a42fD28C976A61Df5D00D0599C34c4f90748c8"; break
            case "ropsten":
            case "ropsten-fork":
                lendingPoolAddressesProviderAddress = "0x1c8756FD2B28e9426CDBDcC7E3c4d64fa9A54728"; break
            case "kovan":
            case "kovan-fork":
                lendingPoolAddressesProviderAddress = "0x506B0B2CF20FAA8f38a4E2B524EE43e1f4458Cc5"; break
            default:
                throw Error(`Are you deploying to the correct network? (network selected: ${network})`)
        }

        await deployer.deploy(Flashloan, lendingPoolAddressesProviderAddress, lendingPoolAddressesProviderAddressV2);
        const deployedFlashloan = await Flashloan.deployed();
        // Replace params of flashloan function with the liquidation params (collateral, debtToLiquidate, liquidableAssetAddress, userAddress, receiveInaTokens)
        await deployedFlashloan.flashloan("0x4Fabb145d64652a948d72533023f6E7A623C7C53", web3.utils.toWei('126621'), liquidableAssetAddress, "0xff049ec2f92e167a0aac6bea0fb11ec6a9abfc1b", false, false);
        await deployedFlashloan.withdraw(liquidableAssetAddress);
    } catch (e) {
        console.log(`Error in migration: ${e.message}`)
    }
}