pragma solidity ^0.6.6;

import "./aave/FlashLoanReceiverBase.sol";
import "./aave/ILendingPoolAddressesProvider.sol";
import "./aave/ILendingPool.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";

contract Flashloan is FlashLoanReceiverBase {

    address public lendingPoolAddressProvider;
    address public lendingPoolAddressProviderV2;
    
    address public asset;
    address public collateral;
    address public user;
    uint256 public purchaseAmount = uint(-1);
    uint256 public amount;
    bool public receiveaToken;
    bool public isV1;


    constructor(address _addressProvider, address _addressProviderV2) FlashLoanReceiverBase(_addressProvider) public {
        lendingPoolAddressProvider = _addressProvider;
        lendingPoolAddressProviderV2 = _addressProviderV2;
    }

    /**
        This function is called after your contract has received the flash loaned amount
     */
    function executeOperation(
        address _reserve,
        uint256 _amount,
        uint256 _fee,
        bytes calldata _params
    )
        external
        override
    {
        require(_amount <= getBalanceInternal(address(this), _reserve), "Invalid balance, was the flashLoan successful?");

        //
        // Your logic goes here.
        // !! Ensure that *this contract* has enough of `_reserve` funds to payback the `_fee` !!
        //

        if (isV1 == true) {
            liquidationV1(collateral, asset, user, purchaseAmount, receiveaToken);
        } else {
            liquidationV2(collateral, asset, user, purchaseAmount, receiveaToken);
        }

        uint totalDebt = _amount.add(_fee);
        transferFundsBackToPoolInternal(_reserve, totalDebt);
    }

    /**
        Flash loan 1000000000000000000 wei (1 ether) worth of `_asset`
     */
    function flashloan(address _asset, uint256 _amount, address _collateral, address _user, bool _recieveaToken, bool _isV1) public onlyOwner {
        bytes memory data = "";
        amount = _amount;
        asset = _asset;
        collateral = _collateral;
        user = _user;
        receiveaToken = _recieveaToken;
        isV1 = _isV1;

        ILendingPool lendingPool = ILendingPool(addressesProvider.getLendingPool());
        lendingPool.flashLoan(address(this), _asset, amount, data);
    }

    function liquidationV1(
        address _collateral, 
        address _reserve,
        address _user,
        uint256 _purchaseAmount,
        bool _receiveaToken
    )
        public
    {
        ILendingPoolAddressesProvider addressProvider = ILendingPoolAddressesProvider(lendingPoolAddressProvider);
  
        ILendingPool lendingPool = ILendingPool(addressProvider.getLendingPool());
        
        require(IERC20(_reserve).approve(address(lendingPool), _purchaseAmount), "Approval error");

        // Assumes this contract already has `_purchaseAmount` of `_reserve`.
        lendingPool.liquidationCall(_collateral, _reserve, _user, _purchaseAmount, _receiveaToken);
    }

    function liquidationV2(
        address _collateral, 
        address _reserve,
        address _user,
        uint256 _purchaseAmount,
        bool _receiveaToken
    )
        public
    {
        ILendingPoolAddressesProvider addressProvider = ILendingPoolAddressesProvider(lendingPoolAddressProviderV2);
  
        ILendingPool lendingPool = ILendingPool(addressProvider.getLendingPool());
        
        require(IERC20(_reserve).approve(address(lendingPool), _purchaseAmount), "Approval error");

        // Assumes this contract already has `_purchaseAmount` of `_reserve`.
        lendingPool.liquidationCall(_collateral, _reserve, _user, _purchaseAmount, _receiveaToken);
    }
}
